﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ClipboardTranslator
{
    using HtmlAgilityPack;
    using System;
    using System.Diagnostics;
    using System.Net;
    using System.Runtime.InteropServices;
    using System.Windows;
    using System.Windows.Controls.Primitives;
    using System.Windows.Input;
    using System.Windows.Interop;
    using System.Windows.Threading;

    public partial class TranslatorWindow : Window
    {
        private const int WM_CLIPBOARDUPDATE = 0x031D;
        private IntPtr windowHandle;
        public event EventHandler ClipboardUpdate;

        private const int GWL_STYLE = (-16);
        private const int WS_MINIMIZEBOX = 0x00020000;
        private const int WS_MAXIMIZEBOX = 0x00010000;

        [DllImport("user32")]
        public static extern Int32 GetWindowLong(IntPtr hWnd, Int32 nIndex);

        [DllImport("user32")]
        public static extern Int32 SetWindowLong(IntPtr hWnd, Int32 nIndex, Int32 dwNewLong);

        private DispatcherTimer dispatcherTimer_;
        private Popup popup_;
        private System.Drawing.Point pos_;

        public TranslatorWindow()
        {
            InitializeComponent();

            this.Loaded += TranslatorWindow_Loaded;
        }

        private void TranslatorWindow_Loaded(object sender, RoutedEventArgs e)
        {
            var hwnd = new WindowInteropHelper(this).Handle;
            var currentStyle = GetWindowLong(hwnd, GWL_STYLE);
            SetWindowLong(hwnd, GWL_STYLE, (currentStyle & ~(WS_MINIMIZEBOX | WS_MAXIMIZEBOX)));

            //this.Hide();
        }

        protected override void OnInitialized(EventArgs e)
        {
            base.OnInitialized(e);

            windowHandle = new WindowInteropHelper(this).EnsureHandle();
            HwndSource.FromHwnd(windowHandle)?.AddHook(HwndHandler);
            Start();
        }

        public static readonly DependencyProperty ClipboardUpdateCommandProperty =
            DependencyProperty.Register("ClipboardUpdateCommand", typeof(ICommand), typeof(TranslatorWindow), new FrameworkPropertyMetadata(null));

        public ICommand ClipboardUpdateCommand
        {
            get { return (ICommand)GetValue(ClipboardUpdateCommandProperty); }
            set { SetValue(ClipboardUpdateCommandProperty, value); }
        }

        protected virtual void OnClipboardUpdate()
        {
            IDataObject iData = Clipboard.GetDataObject();

            string text = "";
            if (iData.GetDataPresent(DataFormats.UnicodeText))
            {
                text = (string)iData.GetData(DataFormats.UnicodeText);
            }
            else if (iData.GetDataPresent(DataFormats.Text))
            {
                text = (string)iData.GetData(DataFormats.Text);
            }

            text = text.Trim();

            if (!String.IsNullOrEmpty(text))
            {
                ShowPopup(text);
                DoTranslate(text);
            }
        }

        public void Start()
        {
            NativeMethods.AddClipboardFormatListener(windowHandle);
        }

        public void Stop()
        {
            NativeMethods.RemoveClipboardFormatListener(windowHandle);
        }

        private IntPtr HwndHandler(IntPtr hwnd, int msg, IntPtr wparam, IntPtr lparam, ref bool handled)
        {
            if (msg == WM_CLIPBOARDUPDATE)
            {
                // fire event
                this.ClipboardUpdate?.Invoke(this, new EventArgs());

                // execute command
                if (this.ClipboardUpdateCommand?.CanExecute(null) ?? false)
                {
                    this.ClipboardUpdateCommand?.Execute(null);
                }

                // call virtual method
                OnClipboardUpdate();
            }
            handled = false;
            return IntPtr.Zero;
        }

        private static class NativeMethods
        {
            [DllImport("user32.dll", SetLastError = true)]
            [return: MarshalAs(UnmanagedType.Bool)]
            public static extern bool AddClipboardFormatListener(IntPtr hwnd);

            [DllImport("user32.dll", SetLastError = true)]
            [return: MarshalAs(UnmanagedType.Bool)]
            public static extern bool RemoveClipboardFormatListener(IntPtr hwnd);
        }

        public void DoTranslate(string text)
        {
            try
            {
                string encText = System.Uri.EscapeUriString(text);
                string url = "https://translate.google.co.kr/?hl=ko#auto/ko/" + encText;
                //System.Diagnostics.Trace.WriteLine("test// url:" + url);
                //Process.Start(url);

                var web = new HtmlWeb();
                var resText = "";
                for (int i = 0; i < 3; i++)
                {
                    var htmlDoc = web.LoadFromBrowser(url, (string s) => {
                        return s != null ? s.Contains("gt-res-content") : false;
                    });
                    var res = (htmlDoc != null && htmlDoc.DocumentNode != null) ? htmlDoc.DocumentNode.SelectSingleNode("//div[@id='gt-res-content']") : null;
                    resText = (res != null) ? res.InnerText.Trim() : "";
                    //System.Diagnostics.Trace.WriteLine("test// result text:" + resText);
                    if (resText.Length > 0)
                        break;
                    System.Threading.Thread.Sleep(100);
                }

                if (resText.Length > 0)
                {
                    ShowResult(resText);
                }
                else
                {
                    ShowPopup("failed translate!");
                }
            }
            catch (Exception e)
            {
                ShowPopup(e.ToString());
            }
        }

        public void ShowResult(string text)
        {
            try
            {
                resultText.Text = text;

                mainWindow.Left = pos_.X;
                mainWindow.Top = pos_.Y + 10;

                //Show();
                Activate();
            }
            catch (Exception e)
            {
                ShowPopup(e.ToString());
            }
        }

        public void ShowPopup(string text)
        {
            if (dispatcherTimer_ != null)
                dispatcherTimer_.Stop();

            if (popup_ == null)
            {
                TextBlock popupText = new TextBlock();
                popupText.Text = " " + text + " ";
                popupText.Background = Brushes.Black;
                popupText.Foreground = Brushes.White;

                popup_ = new Popup();
                popup_.Opened += new EventHandler(this.OpenedPopup);
                popup_.Child = popupText;
            }
            else
            {
                ((TextBlock)popup_.Child).Text = " " + text + " ";
            }
            popup_.IsOpen = false;

            pos_ = System.Windows.Forms.Cursor.Position;

            popup_.HorizontalOffset = pos_.X;
            popup_.VerticalOffset = pos_.Y + 10;
            popup_.IsOpen = true;
        }

        private void OpenedPopup(object sender, EventArgs e)
        {
            if (dispatcherTimer_ == null)
            {
                dispatcherTimer_ = new System.Windows.Threading.DispatcherTimer();
                dispatcherTimer_.Tick += new EventHandler(ClosePopup);
                dispatcherTimer_.Interval = new TimeSpan(0, 0, 1);
            }
            dispatcherTimer_.Start();
        }

        private void ClosePopup(object sender, EventArgs e)
        {
            dispatcherTimer_.Stop();
            popup_.IsOpen = false;
        }
    }
}
